npm i -g typescript               (to install node)
npm init -y                       (for package json file)
npm i -D @types/express typescript nodemon
npm i -D ts-node  (automatically convert ts code to js and run)



generation tsconfig.json
tsc --init

at tsconfig.json replace
"rootDir":"./src"  it means all ts file will be ./src
"outDir":"./dist"  compiled output (js code)  will be added to dist  (all ts code from src will be converted to .dist)


at script use

{
   "dev":"nodemon src/app.ts",
     "build": "tsc -p .",
    "start":"node dist/app.js"
}

//note
For developement mode this commond is useful
 "dev":"nodemon src/app.ts",

For productin mode this command is useful
"build:"tsc -p .",
"start":"node dist/app.js"

nodemon src/app.ts
under the hood nodemon use ts-node package
it just convert ts file to js file and run
this command does not generate js file in dist


"build:"tsc -p .",
it means in our main directory find tsconfig.json 
and complile (ts to js) as per tsconfig.json
here all ts(file at src) will convert to js(at dist folder)
useful for production


node dist/app.js
this command is use for production to run app


.gitignore
node_modules
dist

